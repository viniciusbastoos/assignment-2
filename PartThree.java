import java.util.Scanner;
public class PartThree{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        
        int one = scan.nextInt();
        int two = scan.nextInt();

        System.out.println(Calculator.add(one, two));
        System.out.println(Calculator.substract(one, two));

        Calculator cal1 = new Calculator();
        Calculator cal2 = new Calculator();

        System.out.println(cal1.multiply(one, two));
        System.out.println(cal2.divide(one, two));
    }
}
