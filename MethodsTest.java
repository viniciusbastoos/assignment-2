public class MethodsTest{
	public static void main(String[] args){
		int x = 5;
		
		/*System.out.println(x);
		methodNoInputNoReturn();
		methodOneInputNoReturn(x + 10);
		methodTwoInputNoReturn(10, 1.5);
		int z = methodNoInputReturnInt();
		double result = sumSquareRoot(9, 5);
		System.out.println(result);*/

		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());

		System.out.println(SecondClass.addOne(50));

		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}

	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}

	public static void methodOneInputNoReturn(int y){
		System.out.println("Inside the method one input no return");
		y = y - 5;
		System.out.println(y);
	}

	public static void methodTwoInputNoReturn(int y, double yy){
		System.out.println(y);
		System.out.println(yy);
	}

	public static int methodNoInputReturnInt(){
		int z = 5;
		System.out.println(z);
		return z;
	}

	public static double sumSquareRoot(int one, int two){
		int three = one + two;
		double result =  Math.sqrt(three);
		return result;
	}
	



}